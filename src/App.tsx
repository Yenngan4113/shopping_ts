import { AddShoppingCart } from "@mui/icons-material";
import { Badge, Drawer, Grid, LinearProgress } from "@mui/material";
import { useQuery } from "react-query";
import { Wrapper } from "./App.styles";

export type CartItemType = {
  id: number;
  category: string;
  description: string;
  image: string;
  price: number;
  title: string;
  amount: number;
};

const getProducts = async (): Promise<CartItemType> =>
  await (await fetch("https://fakestoreapi.com/products")).json();

const App = () => {
  const { data, isLoading, error } = useQuery("products", getProducts);
  console.log(data);

  return <div className="App">hi</div>;
};

export default App;
